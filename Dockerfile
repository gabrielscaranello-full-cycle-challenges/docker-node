FROM node:18-alpine

WORKDIR /usr/src/app

COPY --chown=node:node package.json ./

COPY --chown=node:node package-lock.json ./

RUN npm install --only=production

COPY --chown=node:node src ./

USER $user

EXPOSE 3000

CMD [ "npm", "start" ]
