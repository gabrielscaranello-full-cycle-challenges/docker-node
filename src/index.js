import express from 'express'

import {
  getConnection,
  initDb,
  insertPerson,
  getAllPeople
} from './database.js'

const app = express()
const port = 3000
const connection = getConnection()

initDb(connection)
insertPerson(connection, 'Gabriel Scaranello')

app.get('/', async (_, res) => {
  const people = await getAllPeople(connection)
  const peopleList = people.map((person) => `<li>${person.name}</li>`).join('')
  const template = `<h1>Full Cycle Rocks!</h1><ul>${peopleList}</ul>`

  res.send(template)
})

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})
