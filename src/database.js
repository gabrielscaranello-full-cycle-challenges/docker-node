import mysql from 'mysql'

const config = {
  host: 'db',
  user: 'fullcycle',
  password: 'secret',
  database: 'fullcycle'
}

export const getConnection = () => mysql.createConnection(config)

/**
 * @param {mysql.Connection} connection
 */
export const initDb = (connection) => {
  const createTable = `
    CREATE TABLE IF NOT EXISTS people (
      id INT NOT NULL AUTO_INCREMENT,
      name VARCHAR(255) NOT NULL,
      PRIMARY KEY (id)
    )
  `

  connection.query(createTable)
}

/**
 * @param {mysql.Connection} connection
 * @param {string} name
 */
export const insertPerson = async (connection, name) => {
  const select = `
    SELECT * FROM people
    WHERE name = '${name}'
    LIMIT 1
  `

  connection.query(select, (err, rows) => {
    if (err) throw err
    if (rows.length > 0) return

    const insert = `
      INSERT INTO people (name) VALUES ('${name}')
    `

    connection.query(insert)
  })
}

/**
 * @param {mysql.Connection} connection
 */
export const getAllPeople = async (connection) => {
  return new Promise((resolve, reject) => {
    const query = `
      SELECT id, name FROM people
    `

    connection.query(query, (err, rows) => {
      if (err) return reject(err)
      return resolve(rows)
    })
  })
}
