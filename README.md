# Full Cycle Course - Docker Node Challenge

This repository is part of a series of codes created to meet the challenges of the Full Cycle 3.0 course

## Running

```bash
docker-compose up -d
```

The application will be available at http://localhost:8080
